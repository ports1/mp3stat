#include "statistics.h"
using namespace std;

int statistic::place(int rate) const 
{
  if (rate > 426)
    return 18;
  if (rate > 394)
    return 17;
  if (rate > 362)
    return 16;
  if (rate > 330)                                                               
    return 15;                                                                  
  if (rate > 298)                                                               
    return 14;                                                                  
  if (rate > 266)                                                               
    return 13;                                                                  
  if (rate > 234)                                                               
    return 12;                                                                  
  if (rate > 210)
    return 11;
  if (rate > 175)                                                               
    return 10;                                                                  
  if (rate > 140)                                                               
    return 9;                                                                   
  if (rate > 120)                                                               
    return 8;                                                                   
  if (rate > 105)                                                               
    return 7;                                                                   
  if (rate > 87)                                                                
    return 6;                                                                   
  if (rate > 70)                                                                
    return 5;                                                                   
  if (rate > 60)                                                                
    return 4;
  if (rate > 53)
    return 3;
  if (rate > 45)
    return 2;
  if (rate > 35)
    return 1;
  return 0;
}

void statistic::addBit(int b)
{
  amnt_bitrates[place(b)]++;
  bitrates.push_back(b);
  total_frames++;
  total_bitrate += b;
}

void statistic::copy(const statistic &s)
{
   name = s.name;
   total_frames = s.total_frames;
   total_bitrate = s.total_bitrate;
   size = s.size;
   for(int x=0;x<19;x++) amnt_bitrates[x] = s.amnt_bitrates[x];
   bitrates = s.bitrates;
}
