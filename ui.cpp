#include "ui.h"

using namespace std;


char *ui::kbit_array[19] = { 
             "32kbit", "40kbit", "48kbit", "56kbit", "64kbit", "80kbit",        
             "96kbit", "112kbit", "128kbit", "160kbit", "192kbit", "224kbit",   
             "256kbit", "288kbit", "320kbit", "352kbit", "384kbit", "416kbit",  
             "448kbit"  };   

void ui::use() const 
{
   printf("mp3stat : version %s\n",VERSION);
   printf("Options :\n          -l/-L/--list  List available plugins\n");
   printf("          -u/-U/--ui <ui moniker>  select specific UI \n");
}

void ui::ver() const 
{
   printf("mp3stat %s\n", VERSION);
}
