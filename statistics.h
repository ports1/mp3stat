#ifndef _STATISTICS_H
#define _STATISTICS_H
#include <string>
#include <vector>

class statistic
{
  public:
    statistic(const char *n) : name(n), total_frames(0), total_bitrate(0.0), size(0.0)
                         {for(int x=0;x<19;x++) amnt_bitrates[x] = 0;}
    statistic() : name(""), total_frames(0), total_bitrate(0.0), size(0.0)
                         {for(int x=0;x<19;x++) amnt_bitrates[x] = 0;}
    virtual ~statistic() {bitrates.clear();}
    statistic &operator=(const statistic &s) {
              copy(s);
	      return *this;
              }
    inline std::string getName() const { return(name); }
    inline double getAvgbit() const { return((total_bitrate/total_frames)*1000); }
    inline int getTframes() const { return(total_frames); }
    inline double getTbit() const { return(total_bitrate); }
    inline double getSec() const { return((size*1024*8) / getAvgbit()); }
    inline double getSize() const { return(size); }
    inline std::vector<int> getBitrates() const { return(bitrates); }
    inline int getAmntbit(int n) const { return(amnt_bitrates[n]); }

    void addBit(int);
    void setSize(double s) { size = s; }
    int place(int) const;
        
  private:
    std::string name;
    int total_frames;
    double total_bitrate;
    double size;
    int amnt_bitrates[19];
    std::vector<int> bitrates;
    void copy(const statistic &);
};

#endif
