#ifndef _OGGSTAT_H
#define _OGGSTAT_H

#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string>
#include <vorbis/codec.h>

#include "input.h"


class vorb : public input
{
   public:
      vorb () : input(), type("application/ogg") {;}
      virtual ~vorb() {;}
      inline virtual std::string getType() const {return(type); }
      virtual void statfile(statistic *);
   private:
      const std::string type;
      void scan_ogg(FILE *, int, statistic *);
      int test_header (FILE *, ogg_sync_state *, ogg_stream_state *, 
                       vorbis_info *, vorbis_comment *, long *);
};

#endif
