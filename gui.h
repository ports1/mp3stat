#ifndef _GUI_H
#define _GUI_H
#include "ui.h"


/* 
   Public UI interface class, abstracting plugins from actual base ui 
   class.  This is to allow the UI class to be modified if needed without
   needing to recompile plugins, as this header will never change.
*/

class gui : public ui
{
  public:
    gui() : ui() {;}
    virtual ~gui() {;}
    virtual std::string getType() const = 0;
    virtual int start(std::vector<const char*>) = 0;
};

typedef gui* createu_t();
typedef void destroyu_t(gui*);

#endif
