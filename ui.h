#ifndef _UI_H
#define _UI_H
#include <string>
#include <vector>

class ui 
{
  public:
    ui() {;}
    virtual ~ui() { ; }
    void use() const;
    void ver() const;
    inline const char *getKbit(int n) const { return (kbit_array[n]);}
    void setargs(int ac, char **av) { argcc = ac; argvv = av;}
    
    
  protected:
    static char *kbit_array[19];
    char **argvv;
    int argcc;
};

#endif
