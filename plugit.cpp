#include <string>
#include <magic.h>
#include "plugit.h"

using namespace std;

/*   
     Must make sure UI plugins need no actual code from classes it depends on
     in source files.  This means the plugin lists, get_file_data and other
     such functions in ui class.
*/

std::vector<input*>  Inplugins;                                                    
std::vector<gui*>    Uiplugins;

/*
	Calls the backend for file data 
*/
statistic get_file_data (const char *n)
{
  magic_t cookie;
  statistic newfile(n);
  
  cookie = magic_open(MAGIC_MIME);
  magic_load(cookie, NULL);
  vector<input*>::iterator i;
  for(i = Inplugins.begin();i != Inplugins.end(); i++){
    if(!strcmp(magic_file(cookie, n), (*i)->getType().c_str())){
      magic_close(cookie);
      *((statistic*)*i) = newfile;
      (*i)->statfile ((statistic*)*i);
      return(*((statistic*)(*i)));
    }
  }
  return(newfile);
}
