#include <stdio.h>
#include "cli.h"

using namespace std;


/*
   Future command line will change batch mode to be a type of script 
   helper in the way that the gtk ui was in earlier versions. 
   Current console ui output will be achieved with no additional argument
   just supply the files/wildcard/directory.  
*/

void
console::usage() const
{
  use();
  printf ("console UI version %s \n", VERSION);
  printf ("Usage:\nmp3stat (options) \n	-h/-H/--help");
  printf ("	            This help screen\n	-v/-V/-");
  printf ("-version	            Display version\n    ");
  printf ("    -b/-B/--batch <directory or wildcard or files> \n");
}

void
console::print_info()
{
  int                first = -1;
  int                last = -1;
  int                a;

  printf("%s:\n", Input.getName().c_str());
  printf ("  Average bit rate: %.2f kbps\n",	      
	             Input.getAvgbit() / 1000);
  printf ("            Length: %.2f seconds\n",
	             Input.getSec());
  printf ("            Frames: %d\n", Input.getTframes());
  for (a = 0; a < 19; a++) {
    if (Input.getAmntbit(a)>=1) {
       last = a;
       if (first < 0) {
          first = a;
       }
    }
  }
  if (first >= 0 && last >= 0) {
     for (a = first; a <= last; a++) {
	printf ("  %18s: %4.1f%% (%d frames)\n", getKbit(a), 
		       (100.0f * Input.getAmntbit(a)) / Input.getTframes(), 
		       Input.getAmntbit(a)); 
     }
  }
  printf ("\n");
}

int
console::start (vector<const char*> args) 
{
  statistic tempIn;
  vector<const char*>::iterator i;
  string path1;
  struct dirent **test1;
  int index1 = -1;
  for(i = args.begin();i != args.end(); i++){
     if(!strcmp(*i,"-h") || !strcmp(*i, "-H") || !strcmp(*i,"--help")){
        usage();
	return(0);
     }
     if(!strcmp(*i,"-v") || !strcmp(*i, "-V") || !strcmp(*i, "--version")){
        ver();
        printf("Console UI %s\n",VERSION);
	return(0);
     }
  }
  for(i = args.begin();i != args.end(); i++){
    if(!strcmp(*i,"-b") || !strcmp(*i,"-B") || !strcmp(*i,"--batch")){
       i++;
       if(i == args.end()) { 
          usage();
	  return(0);
       }
       index1 = scandir(*i,&test1,0,0);
       if(index1>0){
         for(int n = 0; n < index1;n++){
            if(test1[n]->d_type == DT_REG) {
	       path1 = *i;
	       path1.append("/");
	       path1.append(test1[n]->d_name);
	       tempIn = get_file_data(path1.c_str());
	        if(tempIn.getTframes() > 0) {
	          Input = tempIn;
		  print_info();
	        }
	    }
	    free(test1[n]);
         }
         free(test1);
       } else {	 
          for(;i != args.end();i++){
             tempIn = get_file_data (*i);
             if(tempIn.getTframes() >0){
	        Input = tempIn;
		print_info();
	     }
	  }
       }
       return 0; 
    }    
  }
  usage();
  return (0);
}
extern "C" gui* createu() {
    return new console;
}

extern "C" void destroyu(gui* tempUI) {
    delete tempUI;
}
