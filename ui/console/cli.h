#ifndef _CONSOLEUI_H
#define _CONSOLEUI_H
#include "plugit.h"
#include <dirent.h>

class console : public gui
{
   public:
      console() : gui(), type("console") {;}
      virtual ~console() {;}
      inline virtual std::string getType() const { return (type); }
      virtual int start(std::vector<const char*>);
      virtual void usage() const;
   
   private:
      const std::string type;
      statistic             Input;
      void print_info();
      
};

#endif
