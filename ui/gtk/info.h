#ifndef _INFOCLASS_H
#define _INFOCLASS_H

#include <gtk/gtk.h>
#include "plugit.h"


/*
	structure used to hold all the data
	specific to one side of the program.
	
	graph is container for pixmap
	selected_oldfile is current dir
	fvbox is frame for table2
	fvbox2 is frame for table3
	table2 is bitrate data table
	table3 is file summary table
	set identifies if we need to delete
	zz identifies place in graph 
*/
class               Info
{
public:
  Info () : mp3infoa(""), graph (NULL), selected_oldfile (""), bbreak (NULL), 
            fsum (NULL), table2 (NULL), table3 (NULL), detailw(NULL), 
	    graphtip(NULL), zz (0) {;}
  ~Info () { ; }
  statistic          mp3infoa;
  GtkWidget          *graph;
  std::string        selected_oldfile;
  GtkWidget          *bbreak;
  GtkWidget          *fsum;
  GtkWidget          *table2;
  GtkWidget          *table3;
  GtkWidget          *detailw;
  GtkTooltips        *graphtip; 
  int                 zz;
  Info &operator=(const Info &i){
            copy(i);
  	    return *this;
   }
   private :
      void copy(const Info &);
};

#endif
