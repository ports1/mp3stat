#include "info.h"
using namespace std;

void Info::copy(const Info &i)
{
     mp3infoa = i.mp3infoa;                                               
     graph = i.graph;                                                     
     selected_oldfile = i.selected_oldfile;                               
     bbreak = i.bbreak;                                                   
     fsum = i.fsum;                                                       
     table2 = i.table2;                                                   
     table3 = i.table3;                                                   
     detailw = i.detailw;                                                 
     graphtip = i.graphtip;
     zz = i.zz;                                                           
}
