#include "gtk2_ui.h"

using namespace std;

extern void *tryit;

void
gtk2_ui::clear_detailed (Info * chosen1)
{
   gtk_widget_destroy(chosen1->detailw);
   chosen1->detailw = NULL;
   gtk_tooltips_enable(chosen1->graphtip);
}

void
gtk2_ui::detailed_info (GtkWidget * dummy, GdkEventButton * event, Info * chosen1)
{
  GtkWidget          *lgraph = NULL;
  GdkRectangle        rect;
  GdkGC              *lgc = NULL;
  GdkPixmap          *pixmap = NULL;
  float               x = 0;
  int                 avg = 0, y = 0, zy = 0, zz = 0;
  float               divisor = 0;
  vector<int>         tempBit;

  if ( chosen1->detailw != NULL || chosen1->selected_oldfile == "") {
    return;
  }
  gtk_tooltips_disable(chosen1->graphtip);
  divisor = (float) chosen1->mp3infoa.getTframes() / 800;

  chosen1->detailw = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (chosen1->detailw), 
                        chosen1->selected_oldfile.c_str());
  gtk_window_set_resizable (GTK_WINDOW (chosen1->detailw), FALSE);

  lgraph = gtk_drawing_area_new ();
  gtk_drawing_area_size (GTK_DRAWING_AREA (lgraph), 800, 300);

  gtk_container_border_width (GTK_CONTAINER (chosen1->detailw), 10);
  gtk_container_add (GTK_CONTAINER (chosen1->detailw), lgraph);
  gtk_widget_show (lgraph);
  gtk_widget_show (chosen1->detailw);

  lgc = gdk_gc_new (lgraph->window);
  gdk_gc_set_fill (lgc, GDK_SOLID);
  
  pixmap = gdk_pixmap_new (lgraph->window, 800, 300, -1);

  gdk_draw_rectangle (pixmap, lgraph->style->light_gc[0],
		      TRUE, 0, 0, 800, 300);
  gdk_draw_pixmap (lgraph->window, lgraph->style->white_gc, pixmap,
		   0, 0, 0, 0, 800, 300);
  gdk_gc_set_foreground (lgc, &color_array[0]);
  rect.x = 0;
  rect.y = 0;
  rect.height = 300;
  rect.width = 800;
  tempBit = chosen1->mp3infoa.getBitrates();
  for (int h = 0; h < 19; h++) {
    gdk_draw_line (pixmap, lgraph->style->black_gc, 0, h * 15, 800,
		   h * 15);
  }

  for (x = 0; x < chosen1->mp3infoa.getTframes(); x += divisor) {
    for (y = (int) x;
	 (y < (x + divisor)) && (y < chosen1->mp3infoa.getTframes()); y++) {
      avg += chosen1->mp3infoa.place (tempBit[y]);
      zy++;
    }
    avg /= zy;
    zy = 0;
    gdk_gc_set_foreground (lgc, &color_array[avg]);
    if (zz < 800) {
      gdk_draw_rectangle (pixmap,
			  lgc, TRUE, zz, 300 - (avg * 15), 1, avg * 15);

      gdk_draw_pixmap (lgraph->window,
		       lgc,
		       pixmap,
		       zz, 300 - (avg * 15), zz, 300 - (avg * 15),
		       1, avg * 15);
    }
    avg = 0;
    zz++;
  }
  g_signal_connect (GTK_OBJECT (lgraph), "expose_event",
		    G_CALLBACK (fexpose_event), pixmap);
  g_signal_connect_swapped (GTK_OBJECT (chosen1->detailw), "destroy",
			    G_CALLBACK (fclear_detailed), chosen1);
  gtk_widget_queue_draw (lgraph);
}
