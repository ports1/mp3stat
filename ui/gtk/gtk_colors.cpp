#include "gtk2_ui.h"


/*
	assigns colors to graph color array.  
	Some may not be correctly commented.
*/

void
gtk2_ui::set_graph_colors ()
{
  // for 32 kbit  (dark purple)
  color_array[0].red = (int) (0.25 * 0xffff);
  color_array[0].blue = (int) (0.0 * 0xffff);
  color_array[0].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[0], FALSE, TRUE);
  // for 40 kbit  (light pink)
  color_array[1].red = (int) (0.30 * 0xffff);
  color_array[1].blue = (int) (0.0 * 0xffff);
  color_array[1].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[1], FALSE, TRUE);
  // for 48 kbit (red)
  color_array[2].red = (int) (0.44 * 0xffff);
  color_array[2].blue = (int) (0.0 * 0xffff);
  color_array[2].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[2], FALSE, TRUE);
  // for 56 kbit (yellow)
  color_array[3].red = (int) (0.58 * 0xffff);
  color_array[3].blue = (int) (0.0 * 0xffff);
  color_array[3].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[3], FALSE, TRUE);
  // for 64 kbit (green)
  color_array[4].red = (int) (0.72 * 0xffff);
  color_array[4].blue = (int) (0.0 * 0xffff);
  color_array[4].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[4], FALSE, TRUE);
  // for 80 kbit (light red)
  color_array[5].red = (int) (0.86 * 0xffff);
  color_array[5].blue = (int) (0.0 * 0xffff);
  color_array[5].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[5], FALSE, TRUE);
  // for 96 kbit (dark teal)
  color_array[6].red = (int) (1.0 * 0xffff);
  color_array[6].blue = (int) (0.0 * 0xffff);
  color_array[6].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[6], FALSE, TRUE);
  // for 112 kbit (violet)
  color_array[7].red = (int) (0.0 * 0xffff);
  color_array[7].blue = (int) (0.0 * 0xffff);
  color_array[7].green = (int) (0.30 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[7], FALSE, TRUE);
  // for 128 kbit (purple)
  color_array[8].red = (int) (0.0 * 0xffff);
  color_array[8].blue = (int) (0.0 * 0xffff);
  color_array[8].green = (int) (0.44 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[8], FALSE, TRUE);
  // for 160 kbit (orange)
  color_array[9].red = (int) (0.0 * 0xffff);
  color_array[9].blue = (int) (0.0 * 0xffff);
  color_array[9].green = (int) (0.58 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[9], FALSE, TRUE);
  // for 192 kbit (pale jade)
  color_array[10].red = (int) (0.0 * 0xffff);
  color_array[10].blue = (int) (0.0 * 0xffff);
  color_array[10].green = (int) (0.72 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[10], FALSE, TRUE);
  // for 224 kbit (light teal)
  color_array[11].red = (int) (0.0 * 0xffff);
  color_array[11].blue = (int) (0.0 * 0xffff);
  color_array[11].green = (int) (0.86 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[11], FALSE, TRUE);
  // for 256 kbit (yellow)
  color_array[12].red = (int) (0.00 * 0xffff);
  color_array[12].blue = (int) (0.0 * 0xffff);
  color_array[12].green = (int) (1.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[12], FALSE, TRUE);
  // for 288 kbit (dark red)
  color_array[13].red = (int) (0.0 * 0xffff);
  color_array[13].blue = (int) (0.44 * 0xffff);
  color_array[13].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[13], FALSE, TRUE);
  // for 320 kbit (ugly)
  color_array[14].red = (int) (0.0 * 0xffff);
  color_array[14].blue = (int) (0.58 * 0xffff);
  color_array[14].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[14], FALSE, TRUE);
  // for 352 kbit (purple )
  color_array[15].red = (int) (0.0 * 0xffff);
  color_array[15].blue = (int) (0.72 * 0xffff);
  color_array[15].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[15], FALSE, TRUE);
  // for 384 kbit (bright lime)
  color_array[16].red = (int) (0.0 * 0xffff);
  color_array[16].blue = (int) (0.86 * 0xffff);
  color_array[16].green = (int) (0.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[16], FALSE, TRUE);
  // for 416 kbit (dark pink)
  color_array[17].red = (int) (0.0 * 0xffff);
  color_array[17].blue = (int) (1.0 * 0xffff);
  color_array[17].green = (int) (1.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[17], FALSE, TRUE);
  // for 448 kbit (white)
  color_array[18].red = (int) (1.0 * 0xffff);
  color_array[18].blue = (int) (1.0 * 0xffff);
  color_array[18].green = (int) (1.0 * 0xffff);
  gdk_colormap_alloc_color (cmap, &color_array[18], FALSE, TRUE);
}
