#ifndef _GTK2_UI_H
#define _GTK2_UI_H
#include <gtk/gtk.h>
#include "plugit.h"
#include "info.h"

extern void *tryit;

class gtk2_ui : public gui
{
   public:
      gtk2_ui() : gui(), type("gtk2"),cmap(NULL), file_selector_menu(NULL)
                  {;}
      virtual ~gtk2_ui() {;}
      inline virtual std::string getType() const { return (type); }
      virtual int start(std::vector<const char*>);
      virtual void usage() const;
      
      static void fclear_file_selector(GtkWidget *n, GtkWidget *d) {
                  ((gtk2_ui*)(tryit))->clear_file_selector();
                  }
      static void fis_mp3(GtkWidget* n, Info* m) {
                  ((gtk2_ui*)(tryit))->is_mp3(n,m);
		  }
      static gboolean fexpose_event(GtkWidget * n, GdkEventExpose*m,
                                    GdkPixmap *o) {
		      return ((gtk2_ui*)(tryit))->expose_event(n,m,o);
	              }
      static void fclose_application(GtkWidget* n, GtkWidget *m) {
                  ((gtk2_ui*)(tryit))->close_application(n);
		  }
      static void fcreate_file_select(GtkWidget *n, Info *m) {
                  ((gtk2_ui*)(tryit))->create_file_select(n,m);
		  }
      static void fdetailed_info (GtkWidget *n, GdkEventButton *m,Info *o){
                  ((gtk2_ui*)(tryit))->detailed_info(n,m,o);
		  }
      static void fclear_detailed (Info *n) {
                  ((gtk2_ui*)(tryit))->clear_detailed(n);
                  }
   private:
      const std::string type;
      GdkColor          color_array[19];
      GdkColormap       *cmap;
      GtkWidget         *file_selector_menu;
      gboolean          expose_event (GtkWidget *, GdkEventExpose *, 
                             GdkPixmap *);
      void              clear_detailed(Info*);

      void              set_graph_colors();
      void              detailed_info (GtkWidget *, GdkEventButton *, Info *);
      void              draw_graph(Info*);
      void              draw_keys(Info*);
      void              text_output(Info*);
      void              is_mp3(GtkWidget*, Info *);
      void              clear_file_selector ();
      void              create_file_select (GtkWidget *, Info*);
      void              close_application(GtkWidget *);
      int               gtk_interface();
    
};

#endif

