#ifndef _INPUT_H
#define _INPUT_H
#include "statistics.h"

/* 
    Public input class, used to abstract plugins from the actual input
    class.  This will allow editing of the base input class without 
    requiring plugins to recompile against current mp3stat headers. 
*/

class input : public statistic
{
  public:
    input() : statistic() {;}
    virtual ~input() {;}
    virtual std::string getType() const = 0;
    virtual void statfile (statistic *) = 0;
};

typedef input* createi_t();
typedef void destroyi_t(input*);

#endif
