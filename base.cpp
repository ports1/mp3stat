#include <stdio.h>
#include <dlfcn.h>
#include <dirent.h>

#include "gui.h"
#include "input.h"

using namespace std;

extern std::vector<input*> Inplugins;
extern std::vector<gui*>    Uiplugins;

int main (int argc, char *argv[])
{
  int                 index = 0;                                                 
  void                *plug = NULL;                                              
  createi_t            *inst = NULL;                                              
  destroyi_t           *destr = NULL;                                              
  createu_t            *instu = NULL;
  destroyu_t           *destru = NULL;
  struct dirent              **test;          
  string              uidef;                                   
  vector<gui*>::iterator i;
  vector<const char *>  args;
  vector<const char *>::iterator ar;
  
  index = scandir(PLUGINP, &test, 0, 0);                                         
  for(int n = 0; n< index;n++){                                                  
    if (!strncmp(test[n]->d_name, "input",5)){                                    
       plug = dlopen(test[n]->d_name, RTLD_LAZY);                                
       if (!plug) {
           printf("couldn't load plugin\n%s",dlerror());
	   return -1;                                 
       }
       inst = (createi_t*) dlsym(plug, "createi");                                
       destr = (destroyi_t*) dlsym(plug, "destroyi");                             
       if (!inst || !destr) {
           printf("Couldn't read symbols\n%s",dlerror());
	   return(-1);                       
       }                    
       Inplugins.push_back(inst());           
       free(test[n]);
       plug = NULL;
       inst = NULL;
       destr = NULL;                                                            
    } else                                                                            
    if (!strncmp(test[n]->d_name, "ui",2)){                                       
       plug = dlopen(test[n]->d_name, RTLD_LAZY);                                        
       if(!plug) {
          printf("couldn't load plugin\n%s",dlerror());
	  return(-1);                                                      
       }
       instu = (createu_t*) dlsym(plug, "createu");                                 
       destru = (destroyu_t*) dlsym(plug, "destroyu");                              
       if(!instu || !destru) {
          printf("couldn't read symbols \n %s",dlerror());
	  return(-1);                                            
       }
       Uiplugins.push_back(instu());
       free(test[n]);
       plug = NULL;
       instu = NULL;
       destru = NULL;
    } else {
       free (test[n]);
    }
  }
  free (test);
  
  /* If we dont have any UI plugins, not worth running at all */
  if(Uiplugins.empty()) return -1;
  
  /* Set the default UI to the gtk2 ui shipped with mp3stat
     only if it was built at the time mp3stat was built.
     This should be made a compile time option later on */
  if(!strcmp(GUI,"build")) uidef = "gtk2";
  else uidef = "   ";
      
  /*  If we dont supply any arguments, try default UI 
        If there is no default or if the default is not found 
	then we select the first ui in the vector list */
  if (argc < 2){
    for (i = Uiplugins.begin();i != Uiplugins.end(); i++){
      if (!strcmp((*i)->getType().c_str(), uidef.c_str())){
	 (*i)->start(args);
	 return 0;
      }
    }
    (*(Uiplugins.begin()))->start(args);
    return 0;
  }
  
  /* push all arguments onto argument vector for use in UI's */
  for(int a = 1; a <= argc - 1; a++)
    args.push_back(argv[a]);
  /* Check if user wants to see available UI's */
  for(ar = args.begin(); ar != args.end(); ar++){
    if (!strcmp((*ar), "-l") || !strcmp((*ar), "-L") || 
        !strcmp((*ar),"--list")){
      printf(" Available UI plugins (to be supplied with -u/-U/--ui)\n");
      printf("      NOTE: only first will be used if multiple given\n\n");
      printf("Found :    Plugin Moniker\n");
      for(i = Uiplugins.begin(); i != Uiplugins.end(); i++){
        printf("           %s\n", (*i)->getType().c_str());
      }
      return (0);
    }
  }
    
  /* User has supplied multiple arguments, check if it is a ui 
       If not then select a default one, or the first in list if no 
       default exists.  
  */
  for(ar = args.begin(); ar != args.end(); ar++){
    if (!strcmp((*ar), "-u") || !strcmp((*ar), "-U") || 
        !strcmp((*ar), "--ui")){
      ar++;
      for(i = Uiplugins.begin(); i != Uiplugins.end(); i++){
	if(!strcmp((*i)->getType().c_str(), (*ar))){
	  (*i)->start(args);
	  return 0;
	}
      }
      printf(" Requested plugin not found, please list available \n");
      printf(" with -l/-L/--list\n");
      return(-1);
    }
  }
  for (i = Uiplugins.begin();i != Uiplugins.end(); i++){
    if (!strcmp((*i)->getType().c_str(), uidef.c_str())){
      (*i)->start(args);
      return 0;
    }
  }
  (*(Uiplugins.begin()))->start(args);
  return 0;
}

