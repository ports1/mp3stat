#Build environment variables 

#This uses your environment build flag.
#since we're using c++, we use CXXFLAGS since that's standard.
#If you only set CFLAGS, you should also look into setting CXXFLAGS

BFLAGS		   = $(CXXFLAGS)

#To disable compiling gtk ui plugin, comment out next line
GTK		 = build

#To disable compiling text ui plugin, comment out next line
TEXT             = build

#To disable compiling vorbis plugin, comment out next line
VORBIS		 = build

#To disable compiling mp3 plugin, comment out next line
MP3              = build

INSTALLDIR = /usr/local

CPPFLAGS      = -Wall -DVERSION=\"$(VER)\" 

CXX           = g++

include Makefile.rules
