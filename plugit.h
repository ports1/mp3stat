#ifndef _PLUGIT_H
#define _PLUGIT_H

#include "input.h"
#include "gui.h"

statistic get_file_data(const char*);

#endif
